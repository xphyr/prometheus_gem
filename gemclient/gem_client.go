package gemclient

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"regexp"
	"strings"

	"github.com/prometheus/common/log"
)

const (
	gem_read_single_packet = "^^^APIVAL"
	gem_read_serial_num    = "^^^RQSSRN"
	gem_start_timed_send   = "^^^RQS"
)

// ISIClient is used to connect to an EMC Isilon Cluster
type GEMClient struct {
	gemAddress    string
	gemSerial     string
	gemConnection net.Conn
}

// NewIsiClient returns an initialized Isilon Client.
func NewGEMClient(target string) (*GEMClient, error) {

	log.Debugln("Init ISI Client")

	c := ISIClient{
		UserName:       user,
		Password:       pass,
		ClusterAddress: target,
	}

	reqStatusURL := "https://" + c.ClusterAddress + ":8080/platform/1/cluster/config"

	// make a quick call to the API and ensure that it works
	s := c.CallIsiAPI(reqStatusURL, 2)
	if s != "" {
		c.ClusterName = gjson.Get(s, "name").String()
		c.ISIVersion = gjson.Get(s, "onefs_version.release").String()
		c.NumNodes = gjson.Get(s, "devices.#").Int()

		return &c, nil
	}

	return nil, errors.New("Error creating connection")

}

func readGEM(conn net.Conn) (string, error) {
	fmt.Fprintf(conn, gemread)
	message, err := bufio.NewReader(conn).ReadString('\n')
	if err != nil {
		fmt.Println("Error reading data from GEM")
		return "", fmt.Errorf("error")
	}
	message = strings.TrimSpace(message)
	test := validatePacket(message)
	if !test {
		return "", fmt.Errorf("packet not valid")
	}
	return message, nil
}

func validatePacket(packet string) bool {
	// Validate that the packet starts with "VAL and ends with END"
	fmt.Println("Testing:", packet)
	valid_packet, _ := regexp.MatchString("^VAL.*END$", packet)
	n := 1
	for _, c := range packet {
		if c == ',' {
			n++
		}
	}
	if n != 61 {
		valid_packet = false
	}
	fmt.Printf("Found %v number of fields", n)
	fmt.Println(valid_packet)
	return valid_packet
}
