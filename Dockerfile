# build stage
FROM golang:alpine AS build-env
RUN apk add git
ADD . /src
RUN cd /src && go build -o prometheus_gem

# final stage
FROM alpine
WORKDIR /app
COPY --from=build-env /src/prometheus_gem /app/
EXPOSE 9601
CMD ["/app/prometheus_gem"]