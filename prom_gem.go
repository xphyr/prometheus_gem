package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"net/http"
	"regexp"
	"runtime"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

const gemread = "^^^APIVAL"

var (
	gemCollectionBuildInfo = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "gem_collector_build_info",
			Help: "A metric with a constant '1' value labeled by version, commitid and goversion exporter was built",
		},
		[]string{"version", "commitid", "goversion"},
	)
	gemChannelData = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "gem_channel_data",
			Help: "Channel data, labeled with the channel.",
		},
		[]string{"channel_num"},
	)
	Release = "not defined"
	Commit  = "not defined"
)

type GEMConn struct {
	conn     net.Conn
	serverIP string
}

func (gemconn *GEMConn) readGEM() ([]string, error) {
	// Send command to get a read off the device
	fmt.Fprintf(gemconn.conn, gemread)
	// Read a line from the device
	message, err := bufio.NewReader(gemconn.conn).ReadString('\n')
	if err != nil {
		fmt.Println("Error reading data from GEM")
		gemconn.conn.Close()
		gemconn.connect()
		return nil, fmt.Errorf("error")
	}
	message = strings.TrimSpace(message)
	test := gemconn.validatePacket(message)
	if !test {
		return nil, fmt.Errorf("packet not valid")
	}
	message = strings.TrimPrefix(message, "VAL")
	message = strings.TrimSuffix(message, "END")
	splitString := strings.Split(message, ",")
	return splitString, nil
}

func (gemconn *GEMConn) validatePacket(packet string) bool {
	// Validate that the packet starts with "VAL and ends with END"
	fmt.Println("Testing:", packet)
	valid_packet, _ := regexp.MatchString("^VAL.*END$", packet)
	n := 1
	for _, c := range packet {
		if c == ',' {
			n++
		}
	}
	if n != 61 {
		valid_packet = false
	}
	fmt.Printf("Found %v number of fields", n)
	fmt.Println(valid_packet)
	return valid_packet
}

func init() {
	gemCollectionBuildInfo.WithLabelValues(Release, Commit, runtime.Version()).Set(1)
	prometheus.MustRegister(gemCollectionBuildInfo)
}

func (gemconn *GEMConn) getMetrics(w http.ResponseWriter, r *http.Request) {
	registry := prometheus.NewRegistry()

	gemresponse, _ := gemconn.readGEM()
	for channel_num, val := range gemresponse {
		chName := "channel_" + strconv.Itoa(channel_num)
		val, _ := strconv.ParseFloat(val, 64)
		gemChannelData.WithLabelValues(chName).Set(val)
	}
	registry.MustRegister(gemChannelData)
	h := promhttp.HandlerFor(registry, promhttp.HandlerOpts{})
	h.ServeHTTP(w, r)

}

func (gemconn *GEMConn) connect() {
	// connect to this socket
	log.Printf("Connecting to Green Eye Monitor: %v", gemconn.serverIP)
	myConnection, err := net.Dial("tcp", gemconn.serverIP)
	if err != nil {
		log.Panic("Unable to connect to GEM.  Quiting")
	}

	gemconn.conn = myConnection
	log.Printf("Connection to %v successful.", gemconn.serverIP)
}

func main() {

	gemconn := &GEMConn{
		serverIP: "192.168.5.52:8000",
	}
	gemconn.connect()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
		<head><title>GreenEye Energy Monitor Exporter</title></head>
		<body>
		<h1>GEM Exporter</h1>
		<p><a href="/metrics">Metrics</a></p>
		</body>
		</html>`))
	})

	http.HandleFunc("/metrics", gemconn.getMetrics)
	log.Fatal(http.ListenAndServe(":9601", nil))
}
