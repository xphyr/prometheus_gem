# Prometheus_GEM

## A Prometheus exporter for the GreenEye Energy Monitor

This is a [Prometheus](https://prometheus.io/) exporter for the [GreenEye Energy Monitor](http://www.brultech.com/greeneye/). The GreenEye Energy Monitor (GEM) is a whole-house energy monitor. Its great for high power usage in your home. In theory there is other analysis that could be done, and I have used it to find issues with my furnace in the past by noting that the power usage patterns changed.

For visualization you can use [Grafana](https://grafana.com/).  Included in the repo is an example grafana dashboard file that can be used as a starting point.

## References

https://www.brultech.com/software/files/downloadSoft/GEM-CMD_Commands_ver_10_0.pdf

https://www.brultech.com/software/files/downloadSoft/GEM-PKT_Packet_Format_2_1.pdf